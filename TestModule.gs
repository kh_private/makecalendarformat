/* this module is TestFunction collections */

/*
function testPrintFormatHoliday() {
  Logger.clear();
  var range = sh.getRange( "A57:I64" );
  var date = new Date( '2018-03-18' );
  printFormatHoliday( range, date );
}
*/

function testTableModule_ClassifyHoliday() {
  var mc_table = getTable();
  mc_table.classifyHoliday();
  mc_table.print();
}

function debugDate() {
  mcDate = getLastDate( sh.getDataRange().getDisplayValues() );
  Logger.log( '今日\t\t\t' + mcDate.next().get().toLocaleDateString() );
  Logger.log( '明日\t\t\t' + mcDate.next().get().toLocaleDateString() );
  Logger.log( '明後日の翌日\t' + mcDate.next().next().get().toLocaleDateString() );
}

function testGetDataRange() {
  Logger.log( SpreadsheetApp.getActiveSheet().getDataRange().getA1Notation() );
}

function testGetLastDate() {
  Logger.clear();
  var date = getLastDate(　SpreadsheetApp.getActiveSheet().getDataRange().getDisplayValues() );
  Logger.log( date.get() );
}

function testPrintDate() {
  Logger.clear();
  var range = sh.getDataRange();
  var date = getLastDate();
  printDate( range, date );
}

// this function is debug user cache of decided start_time and end_time in setPlanTime.
function debugProperties() {
  Logger.clear();
  var prop = PropertiesService.getDocumentProperties().getProperties();
  for ( var k in prop ) {
    Logger.log('key => %s , value => %s', k, prop[k] );
  }
}

function testFindTableRows() {
  Logger.clear();
  Logger.log( findTableRows( sh.getRange( 147, 1, 8, 8 ), ['所感']).getA1Notation() );
}

function testFindTableColumns() {
  Logger.clear();
  Logger.log( findTableColumns( sh.getRange( 147, 1, 8, 8 ), '火' ).getA1Notation() );
}

function testGetHolidayColumns() {
  Logger.clear();
  getHolidayColumns( sh.getRange( 111, 1, 8, 8 ) ).forEach( function(range) { Logger.log( range.getA1Notation() ) } );
}