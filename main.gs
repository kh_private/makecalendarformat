/*************
/* @Title   : MainModule
/* @author  : k_hirayama@ap-com.co.jp
/* @update  : 2018/07/03
/* @version : 2.0
/************/

// Main Function
function main() {
  // ログの初期化
  Logger.clear();

  // 新たな表の領域を取得
  var mc_table = getTable();
  
  // テーブルの描写
  mc_table.decorate();
  
}

/***
/* setPlanTime function is setting plan time with UserFormPrompt.
/* this function is use timepicker1.3.5 on jQuery3.3.1 version.
/* @see <a href="https://developers.google.com/apps-script/guides/html/reference/run">google.script.run</a>
/* @see <a href="http://timepicker.co/">jQuery TimePicker</a>
/***/
function setPlanTime() {
  
  // htmlの生成
  var html = '<html><head>';
  html += '<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>';
  html += '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css\"></head><br>';
  html += '<body><script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js\"></script>';
  html += '出社時刻<input id="from_start" value="" class="timepicker text-center" jt-timepicker="" time="model.time" time-string="model.timeString" default-time="model.options.defaultTime" time-format="model.options.timeFormat" start-time="model.options.startTime" min-time="model.options.minTime" max-time="model.options.maxTime" interval="model.options.interval" dynamic="model.options.dynamic" scrollbar="model.options.scrollbar" dropdown="model.options.dropdown" /><br>';
  html += '退社時刻<input id="to_end" value="" class="timepicker text-center" jt-timepicker="" time="model.time" time-string="model.timeString" default-time="model.options.defaultTime" time-format="model.options.timeFormat" start-time="model.options.startTime" min-time="model.options.minTime" max-time="model.options.maxTime" interval="model.options.interval" dynamic="model.options.dynamic" scrollbar="model.options.scrollbar" dropdown="model.options.dropdown" /><br><br>';
  html += '<input id="btnOK" type="button" value="OK" onclick="google.script.run.withSuccessHandler(uiClose).setValue(document.querySelector(\'#from_start\').value, document.querySelector(\'#to_end\').value);" /><br>';
  
  var htmloutput = HtmlService.createHtmlOutput(html)
    .setWidth( 300 )
    .setHeight( 200 )

  // スクリプトの生成
    .append( "<script>$('#from_start').timepicker({timeFormat: \"H:mm\",interval: 15,minTime: \"6:00\",maxTime: \"23:00\", startTime: \"9:00\",dynamic: false,dropdown: true,scrollbar: true });" )
    .append( "$('#from_start').change( () => { $(this).attr( 'value', $(this).val() ) } );" )
    .append( "$('#to_end').timepicker({ timeFormat: \"H:mm\", interval: 15, minTime: \"6:00\", maxTime: \"23:00\", startTime: \"17:30\", dynamic: false, dropdown: true, scrollbar: true });" )
    .append( "$('#to_end').change( () => { $(this).attr( 'value', $(this).val() ) } );" )
    .append( 'function uiClose() { google.script.host.close(); }' )
    .append( "</script></body></html>" );
  
  // dialogの呼び出し
  SpreadsheetApp.getUi().showModalDialog( htmloutput, '予定時刻のセット' );
  
}

/***
/* this function is set property of this document that called by client side javascript from setPlanTime() function.
/* @param s is planning time at start.
/* @param e is planning time at end.
/***/
function setValue( s, e ) {
  PropertiesService.getDocumentProperties()
    .setProperty( 'estimated_start_time', s )
    .setProperty( 'estimated_end_time', e );  

  Logger.log( s + "~" + e );
}