/*************
/* @Title   : DateModule
/* @author  : k_hirayama@ap-com.co.jp
/* @update  : 2018/07/03
/* @version : 2.0
/************/

/* this module is date function collections */

function createMcDateClass() {
  return new mcDateClass();
}

/****
/* mcDateClass is usefully for rehabilitation work management for 【ひな型】リハビリ出社記録_社員名.
/* addition to the original Date Object, functions that can call next day and previous day,
/* and has japanese week name, and to reference to japan holiday on google calendar.
/********/
( function( global ) {

  var mcDateClass = ( function() {

    mcDateClass.name = "mcDateClass";

/*--- constructor ---*/

    /*****
    /* mcDateClass is useful to use calendar, include Date object.
    /* isHoliday property is decide to holiday, and cal property is impliments Google Calendar.
    /* mainly, this class is used by  in TableModule.gs
    /**********/
    function mcDateClass() {
      this.cal = CalendarApp.getCalendarById( 'ja.japanese#holiday@group.v.calendar.google.com' );
      this.isHoliday = false;
      this.year = new Date().getYear();
    }
    
/*--- Static method ---*/
    
    /******
    /* getWeekStart method is reverse search last sunday based on the today (dependence system clock).
    /* @return mcDateClass is last sunday. ( start of week. )
    /************/
    mcDateClass.getWeekStart = function() {
      var mc_date = new mcDateClass();
      mc_date.set( Date.now() );
      do {
        Logger.log( mc_date.previous().get().toLocaleString() );
      } while ( mc_date.getDay() !== '日' );      
      
      return mc_date;

    }
    
/*--- Instance methods ---*/

    /******
    /* next function is return mcDateClass include next day from the set date.
    /* !!! be careful, this class is mutable.
    /* @return mcDateClass
    /**************/
    mcDateClass.prototype.next = function() {
      this.set( this.date.setDate( this.date.getDate() + 1 ) );
      return this;
    };
    
    /******
    /* previous function is return mcDateClass include yesterday from the set date.
    /* !!! be careful, this class is mutable.
    /* @return mcDateClass
    /**************/
    mcDateClass.prototype.previous = function() {
      this.set( this.date.setDate( this.date.getDate() - 1 ) );
      return this;
    };    
    
    /******
    /* get function is return the set date.
    /* @return Date object that representation this instance.
    /**************/
    mcDateClass.prototype.get = function() {
      return this.date;
    };

    /******
    /* getDay function is return japanese weekday of string.
    /* @return String weekday.
    /**************/
    mcDateClass.prototype.getDay = function() {
      return this.weekday;
    };
    
    /******
    /* set function is set date object on mcDateClass. this function also calling _isHoliday private function and set japanese weekday. 
    /* !!! be careful, this class is mutable.
    /* @param date is Date object.
    /* @return after process to mcDateClass.
    /**************/
    mcDateClass.prototype.set = function( date ) {
      this.date = new Date( date );
      this.isHoliday = _isHoliday( this.cal, this.date );
      switch( this.date.getDay() ) {
        case 0:
          this.weekday = '日';
          break;
        case 1:
          this.weekday = '月';
          break;
        case 2:
          this.weekday = '火';
          break;
        case 3:
          this.weekday = '水';
          break;
        case 4:
          this.weekday = '木';
          break;
        case 5:
          this.weekday = '金';
          break;
        case 6:
          this.weekday = '土';
          break;
        default:
          this.weekday = '';
      }
      return this;
    };
    
/*--- Private methods ---*/
    
    /******
    /* _isHoliday function is inner function that so judge holiday or ordinaryday.
    /* @param cal is CalendarApps instance object.
    /* @param date is target to judge by this function.
    /* @return saturday and sunday, and holiday is return true. ordinaryday is return false.
    /**************/
    function _isHoliday( cal, date ) {
      return cal.getEventsForDay( date ).length > 0 || date.getDay() === 6 || date.getDay() === 0 ?
        true :
        false;
    }

    return mcDateClass;
    
  } )();
  
  return global.mcDateClass = mcDateClass;
  
} )( this );

/*-------- below Instance methods ---------*/

function next() {
  throw new Error("このメソッドは直接呼び出せません");
}

function previous() {
  throw new Error("このメソッドは直接呼び出せません");
}

function get() {
  return this.date;
}

function set( date ) {
  throw new Error("このメソッドは直接呼び出せません");
}

function isHoliday() {
  throw new Error("このメソッドは直接呼び出せません");
}

/****
/* print function is for debug to each member in instance.
/* @return
/********/
function print() {
  Logger.log( ' date : ' + mcDateClass.getDate() );
  Logger.log( ' weekday : ' + mcDateClass.getDay() );
  Logger.log( ' isHoliday : ' + mcDateClass.isHoliday() );
}