/*************
/* @Title   : TableModule
/* @author  : k_hirayama@ap-com.co.jp
/* @update  : 2018/07/03
/* @version : 2.0
/************/

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/* !!! This Module include two classes that mcTableFactory and mcTableClass.
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

function getTable() {
  return mcTableFactory.createTable( SpreadsheetApp.getActiveSheet() );
}

function decorate( mc_date ) {
  throw new Error("このメソッドは直接呼び出せません");
}


/******
/* mcTableFactory class is factory to make mcTableClass.
/* like a factory method in design pattern.
/**************/
( function ( global ) {
  
  var mcTableFactory = ( function() {

    mcTableFactory.name = "mcTableFactory";

/*--- constructor ---*/
    function mcTableFactory(){};
    
/*--- static methods ---*/
    
    /****
    /* createTable method is create new mcTable instance at reference last date with new table range.
    /* @return new mcTable.
    /**********/
    mcTableFactory.createTable = function( sheet ) {
      var mc_date = mcTableFactory.getLastDate( sheet.getDataRange().getDisplayValues() );
      var mc_table = new mcTable( sheet );

      // 新規シートの場合は直前の日曜日から開始
      if ( mc_date.get() === undefined ) { mc_date = mcDateClass.getWeekStart() }
        
      // 取得した日付の翌日から１週間分のmcDateClassをTableオブジェクトにセット
      for( var i = 1; i <= 7; i++ ) {
        var d = new createMcDateClass();
        d.set( mc_date.next().get().toLocaleDateString() );
        mc_table.setDate( d );
      }
      
      return mc_table;

    }
    
    /****
    /* getLastDate method is get last date in used range of active spreadsheet.
    /* but, not date type simple regular expression of date like 'MM/DD'.
    /* @param vals is array that into values of string like date format.
    /* @return DateModule instance.
    /************/
    mcTableFactory.getLastDate = function( vals ) {

      var mc_date = new mcDateClass();

      if ( vals.length > 0 ) {
        regex = new RegExp('^[0-9]{1,2}/[0-9]{1,2}$');
        
        // gets date values into '月日' rows.
        this.dates = Array.prototype.concat.apply( [], vals ).filter( function( cell ) {
          if ( regex.test( cell ) ) {
            return true;
          }
        } );

        // number_sortをするためDate型に直す
        this.dates = this.dates.map( function( data_string ) {
          return new Date( mc_date.year + '/' + data_string );
        } );             
        
        // 昇順ソート
        this.dates.sort( function(a, b) {
          return a - b;
        } );
        
        mc_date.date = this.dates[this.dates.length - 1];
      }
      
      return mc_date;
    }
    
    return mcTableFactory;
    
  } )();

  return global.mcTableFactory = mcTableFactory;

} )( this );


/*****
/* mcTable class is concrete one week work schedule management format on spreadsheet.
/**********/
( function( global ) {
  const table_row = 8;
  const table_column = 9;
  const table_left = 1;

  var mcTable = ( function() {

    mcTable.name = "mcTable";

/*--- constructor ---*/
    
    /*****
    /* mcTable class is concrete one week work schedule management calendar format.
    /* @param sheet is spreadsheet that exist to work schedule management calendar format.
    /* @type sheet is same as Sheet of paramater.,
    /* @type range is all used Range on sheet.,
    /* @type current_table is new Range for this instance(mcTable).,
    /* @type prop is permanently Properties that as save data when user selected work start plan time and work end plan time.,
    /* @type dates is Date for a one week in array.,
    /* @type holidays is Range for holiday cells into a one week in array. as a caution, include saturday too.,
    /* @type ordinarydays is Range for not holiday cells into a one week in array.
    /* }
    /**********/
    function mcTable( sheet ) {
      this.sheet = sheet;
      this.range = sheet.getDataRange();
      this.current_table = sheet.getRange( this.range.getLastRow() + 2, table_left, table_row, table_column );
      this.prop = PropertiesService.getDocumentProperties().getProperties();
      this.dates = [];
      this.holidays = [];
      this.ordinarydays = [];
    }
    
/*--- instance methods ---*/

    /****
    /* decorate method is like template method in design pattern but not split class.
    /* this method specify flow of process.
    /* @return be output mcTable instance.
    /**********/
    mcTable.prototype.decorate = function() {

      // 表の書式セット
      this.printFormat();

      // 表に日付の出力
      this.printDate();

      // 平日と祝祭日の仕分け
      this.classifyHoliday();

      // 予定時刻の入力
      this.printPlanTime();
      
      // 祝日の書式セット
      this.printFormatHoliday();
      
      // データ範囲の更新
      this.range = this.sheet.getDataRange();
      
      return this;

    };
    
    /****
    /* setData method is to appending mcDate instance in dates.
    /* @param mc_date is to want append mcDate instance.
    /* @return be processed mcTable instance.
    /**********/
    mcTable.prototype.setDate = function( mc_date ) {
      this.dates.push( mc_date );
      return this;
    };
    
    
/*--- private method ---*/
    
    /****
    /* findRow method is find table rows from row headers.
    /* @param strRowTitles is the set of strings you want to search.
    /*   as a caution, multiple strings are to use for non merge Cell from A:B.
    /* @return is found Range by this method.
    /**********/
    mcTable.prototype.findRow = function( strRowTitles ) {
      var ret;

      // forEachでgetNumRows()グローバルスコープだと怒られるので変数にセット
      current_table = this.current_table;

      strRowTitles.forEach( function( strRowTitle, column ) {
        for( var row = 1; row <= this.current_table.getNumRows(); row++　) {
          if ( this.current_table.getCell( row, column + 1 ).getValue() === strRowTitle ) {
            ret = this.current_table.getCell( row, column + 1 ).offset( 0, 2 - column, table_left, table_column - 2 );
            return;
          }
        }
      }, this );
      return ret;
    };
    
    /****
    /* findColumn method is find table rows from row headers.
    /* @param strColumnTitle is the set of strings you want to search.
    /* @return is found Range by this method.
    /**********/
    mcTable.prototype.findColumn = function( strColumnTitle ) {
      for( var column = 1; column <= this.current_table.getNumColumns(); column++ ) {
        if ( this.current_table.getCell( 1, column ).getValue() === strColumnTitle ) {
          return this.current_table.getCell( 1, column ).offset( 0, 0, table_row );
        }
      }
    };
    
    /****
    /* classifyHoliday method is decide holiday or ordinaryday.
    /*   this method using mcTableClass.dates member into instance.
    /* @return be processed mcTable instance.
    /**********/
    mcTable.prototype.classifyHoliday = function() {
      this.dates.forEach( function( mc_date ) {
        if ( mc_date.isHoliday ) {
          this.holidays.push( this.findColumn( mc_date.getDay() ) );
        } else {
          this.ordinarydays.push( this.findColumn( mc_date.getDay() ) );
        }
      }, this );
      
      return mcTable;
      
    };
    
    /****
    /* getHolidayColumns method is all at once to use range for holiday.
    /* @return RangeList of holiday ranges in one week.
    /**********/
    mcTable.prototype.getHolidayColumns = function() {
      var A1Notations = this.holidays.map( function( range ) { return range.getA1Notation() } );
      return this.holidays.length == 0
        ? false
        : this.sheet.getRangeList( A1Notations );
    };

    /****
    /* getOrdinarydayColumns method is all at once to use range for not holidays.
    /* @return RangeList of not holiday ranges in one week.
    /**********/
    mcTable.prototype.getOrdinarydayColumns = function() {
      var A1Notations = this.ordinarydays.map( function( range ) { return range.getA1Notation() } );
      return this.ordinarydays.length == 0
        ? false
        : this.sheet.getRangeList( A1Notations );
    };

    /****
    /* printDate method is output new dates.
    /* @return be processed mcTable instance.
    /**********/
    mcTable.prototype.printDate = function() {
      var dates = new Array(1);
      dates[0] = [];
      
      this.dates.forEach( function( mc_date ) { 
        dates[0].push( mc_date.get() );
      }, this );
      
      this.findRow( ['月日'] ).setValues( dates );
    };
    
    /****
    /* printPlanTime method is output designated working start time and end time.
    /* @return be processed mcTable instance.
    /**********/
    mcTable.prototype.printPlanTime = function() {
      start_time = this.prop['estimated_start_time']; 
      end_time = this.prop['estimated_end_time'];
      rangelist = this.getOrdinarydayColumns();
      if ( rangelist !== false ) {
        rangelist.getRanges().forEach( function( range ) {
          range.getCell( 3, 1 ).setValue( start_time )
            .offset( 1, 0 ).setValue( end_time );
        } );
      }
    };
    
    /****
    /* printFormat method is output for outline and table format and table view.
    /* @return be processed mcTable instance.
    /**********/
    mcTable.prototype.printFormat = function() {
      
      // 表のフォーマット
      this.current_table     // 表の罫線と文字配置のセット
        .setBorder( true, true, true, true, true, true )
        .setHorizontalAlignment( "center" )
      
      var youbi = this.sheet.getRange( this.current_table.getRow(), this.current_table.getColumn(), 1, 2 );
      youbi.merge()     // 一行目のテンプレート出力
        .setValue( '曜日' )
        .offset( 0, 2, 1, 7 ).setValues( [['日', '月', '火', '水', '木', '金', '土']] );
      
      // 土日のセルカラーをセット
      this.findColumn( '土' ).setBackground( '#d0e0e3' );
      this.findColumn( '日' ).setBackground( '#f4cccc' );
      
      var tsukihi = youbi.offset( 1, 0 ).merge().setValue( '月日' ).setBackground( '#9fc5e8' );
      var yotei = tsukihi.offset( 1, 0, 2, 1 ).merge().setValue( '予定' );
      yotei.offset( 0, 1, 2, 1 ).setValues( [['出社時間'], ['退社時間']] );
      
      var zisseki = yotei.offset( 2, 0, 2, 1 ).merge().setValue( '実績' );
      zisseki.offset( 0, 1, 2, 1 ).setValues( [['出社時間'], ['退社時間']] );
      
      var taichomen = zisseki.offset( 2, 0, 1, 2 ).merge().setValue( '体調面' );
      var shokan = taichomen.offset( 1, 0, 1, 2 ).merge().setValue( '所感' );
      
      this.findRow( ['月日'] )        // 月日行のフォーマット
        .setNumberFormat("m/d")
        .setBackground( '#9fc5e8' );

      shokan.offset( 0, 2, 1, 7 )     // 最終行のフォーマット
        .setHorizontalAlignment( 'left' )
        .setVerticalAlignment( 'top' )
        .setWrapStrategy( SpreadsheetApp.WrapStrategy.CLIP )
        .setWrap( true );

    };
    
    /****
    /* printFormatHoliday method is change cell color of holiday range.
    /**********/
    mcTable.prototype.printFormatHoliday = function() {
      
      // 土日祝祭日セルカラーのセット
      if ( this.holidays.length > 0 ) {
        rangelist = this.getHolidayColumns();
        rangelist.getRanges().forEach( function( range ) {
          if ( range.getCell( 1, 1 ).getValue() == '土' ) {
            range.offset( 0, 0, range.getNumRows(), 1 ).setBackground( '#d0e0e3' )
          } else {
            range.offset( 0, 0, range.getNumRows(), 1 ).setBackground( '#f4cccc' )
          }
        } );
      }
      
      // 月日の行を青色に戻す
      this.findRow( ['月日'] ).setBackground( '#9fc5e8' );
    };
    
    /****
    /* print method is for debug each member in instance.
    /**********/
    mcTable.prototype.print = function() {
      Logger.log( 'sheet => ' + this.sheet.getSheetName() );
      Logger.log( 'range => ' + this.range.getA1Notation() );
      Logger.log( 'current_table => ' + this.current_table.getA1Notation() );
      for( var property in this.prop ) {
        Logger.log( 'prop => ' + property );
      }
//      this.dates.forEach( function( date ) { Logger.log( date.get() ) } );
      this.holidays.forEach( function( range, i ) { Logger.log( 'h' + i + ' ' + range ) } );
      this.ordinarydays.forEach( function( range, i ) { Logger.log( 'o' + i + ' ' + range ) } );
    }

    return mcTable;
    
  } )();
  
  return global.mcTable = mcTable;
  
} )( this );